/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     25/11/2022 10:57:51 a. m.                    */
/*==============================================================*/


/*==============================================================*/
/* Table: agenda_citas                                          */
/*==============================================================*/
create table agenda_citas
(
   id_agenda_cita       bigint not null auto_increment  comment '',
   id_medico            bigint not null  comment '',
   fecha_cita           date not null  comment '',
   hora_cita            time not null  comment '',
   numero_consultorio   int not null  comment '',
   estado_cita          int not null  comment '',
   primary key (id_agenda_cita)
);

/*==============================================================*/
/* Table: medicos                                               */
/*==============================================================*/
create table medicos
(
   id_medico            bigint not null auto_increment  comment '',
   nombre_medico        varchar(200) not null  comment '',
   email_medico         varchar(200) not null  comment '',
   celular_medico       bigint not null  comment '',
   direccion_medico     text not null  comment '',
   registro_medico      varchar(30) not null  comment '',
   primary key (id_medico)
);

alter table agenda_citas add constraint fk_agenda_c_reference_medicos foreign key (id_medico)
      references medicos (id_medico) on delete cascade on update cascade;

